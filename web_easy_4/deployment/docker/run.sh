#!/bin/sh
service apache2 stop
service apache2 start
cd /var/www/html && tar xvfz git.tgz
chmod 777 /var/www/html
cp /root/flag /flag
chown root:root /var/www/html/index.php
/bin/bash
cd /var/www/html && rm index.php && rm .index.php && rm -r .git

#!/usr/bin/env python

import requests
import sys

ip = sys.argv[1]
port = sys.argv[2]
url = 'http://%s:%s/download.php' %(ip,port)
data = {"f":"index.php"}
try:
   r = requests.post(url,params=data)
   if '$flag' in r.content:
       print (True,"OK")
   else:
       print (False,"download check fail")
except Exception,e:
    print (False,str(e))

<?php
require("./flag.php");
function GetIP(){
	if(!empty($_SERVER["HTTP_CLIENT_IP"]))
		$cip = $_SERVER["HTTP_CLIENT_IP"];
	else if(!empty($_SERVER["HTTP_X_FORWARDED_FOR"]))
		$cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
	else if(!empty($_SERVER["REMOTE_ADDR"]))
		$cip = $_SERVER["REMOTE_ADDR"];
	else
		$cip = "";
	return $cip;
}

$IP = GetIP();
echo "Your IP : ".$IP;
echo "<br>";
if ($IP=="127.0.0.1"){
echo $flag;
}
else{
echo "IP forbidden! Only local! <br>";
}
?>

#!/usr/bin/env python

import requests
import sys

ip = sys.argv[1]
port = sys.argv[2]
url = 'http://%s:%s/index.php' %(ip,port)
data = {"hello":"world"}
try:
   r = requests.post(url,data)
   if 'IP forbidden! Only local!' in r.content:
       print (True,"OK")
   else:
       print (False,"ip check fail")
except Exception,e:
    print (False,str(e))

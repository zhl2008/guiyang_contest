### 1.题目名称

Hello Admin!



### 2.题目类型

web(middle)



### 3.题目描述

在一次网络扫描中扫到了这样一个站，你能拿到他的flag吗。



### 4.考察知识点

考察基本的x-forwarded-for



### 5.解题思路

访问一下发现提示IP forbidden! Only local!

尝试利用x-forwarded-for伪造ip为localhost，即127.0.0.1，访问成功

即可拿到flag







